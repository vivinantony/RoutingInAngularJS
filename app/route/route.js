angular.module('AppRoute', []).config(function($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $routeProvider
        .when('/', {
            templateUrl: 'view/home.html',
            controller: 'HomeController'
        })
        .when('/about', {
            templateUrl: 'view/about.html',
            controller: 'AboutController'
        })
        .when('/contact', {
            templateUrl: 'view/contact.html',
            controller: 'ContactController'
        })
});